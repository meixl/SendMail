package com.mei.sendmail;

import java.io.File;
import java.time.LocalDateTime;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/mail")
public class MailController {

    @Value("${spring.mail.host}")
    public String          mail;

    @Value("${spring.mail.username}")
    public String          username;

    @Value("${spring.mail.password}")
    public String          password;

    @Autowired
    private JavaMailSender javaMailSender;

    @RequestMapping("/page")
    public ModelAndView page(ModelAndView mov) {
        mov.setViewName("/page");
        return mov;
    }

    /**
     * 文本发送 
     */
    @RequestMapping("/text")
    public String send1() {

        SimpleMailMessage mailMessage = new SimpleMailMessage();

        // 邮件发送人
        mailMessage.setFrom(username);

        // 邮件收件人
        mailMessage.setTo(username);
        // 邮件标题
        mailMessage.setSubject("文本邮件发送");
        // 邮件正文
        mailMessage.setText("邮件文本内容: " + LocalDateTime.now());

        // 调用 api, 发送邮件
        javaMailSender.send(mailMessage);

        return "文本邮件发送成功: " + LocalDateTime.now();
    }

    /**
     * html 发送
     */
    @RequestMapping("/html")
    public String send2() throws MessagingException {

        MimeMessage mineMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mineMessage, true);

        // 邮件发送人
        mimeMessageHelper.setFrom(username);

        // 邮件收件人
        mimeMessageHelper.setTo(username);
        // 邮件标题
        mimeMessageHelper.setSubject("html 邮件发送");
        // 邮件正文
        mimeMessageHelper.setText("<span style='color:red'>" + LocalDateTime.now() + "</span>", true);

        // 调用 api, 发送邮件
        javaMailSender.send(mineMessage);

        return "html 文本邮件发送成功: " + LocalDateTime.now();
    }

    /**
     * 附件发送 
     */
    @RequestMapping("/attach")
    public String send3() throws MessagingException {

        MimeMessage mineMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mineMessage, true);

        // 邮件发送人
        mimeMessageHelper.setFrom(username);

        // 邮件收件人
        mimeMessageHelper.setTo(username);
        // 邮件标题
        mimeMessageHelper.setSubject("附件邮件发送");
        // 邮件正文
        mimeMessageHelper.setText("这是一封带有附件的邮件" + LocalDateTime.now(), true);

        // 添加附件
        FileSystemResource file = new FileSystemResource(new File("D:\\cookies.txt"));
        String fileName = file.getFilename();
        mimeMessageHelper.addAttachment(fileName, file);

        // 重复添加附件
        mimeMessageHelper.addAttachment(fileName, file);
        mimeMessageHelper.addAttachment(fileName, file);

        // 调用 api, 发送邮件
        javaMailSender.send(mineMessage);

        return "附件邮件发送成功: " + LocalDateTime.now();
    }

    /**
     * 图片发送。代码还是给下。
     * 图片发送我觉得没啥意思，可以把内容转换成 html 来发送。
     */
    @RequestMapping("/pic")
    public String send4() throws MessagingException {

        MimeMessage mineMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mineMessage, true);

        // 文件id, 注意 2 个地方一致
        String fileId = String.valueOf(System.currentTimeMillis());

        // 邮件发送人
        mimeMessageHelper.setFrom(username);

        // 邮件收件人
        mimeMessageHelper.setTo(username);
        // 邮件标题
        mimeMessageHelper.setSubject("图片邮件发送");
        // 邮件正文
        mimeMessageHelper.setText("这是一封带有图片的邮件: <img src='cid:" + fileId + "'></img>", true);

        // 添加附件
        FileSystemResource file = new FileSystemResource(new File("D:\\11111.png"));
        mimeMessageHelper.addInline(fileId, file);

        // 调用 api, 发送邮件
        javaMailSender.send(mineMessage);

        return "附件邮件发送成功: " + LocalDateTime.now();
    }

}