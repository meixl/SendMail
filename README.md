# SendMail

#### 介绍
慕课网 Spring Boot 发送邮件
https://www.imooc.com/learn/1036

#### 测试页面
http://localhost:8080/mail/page

#### 项目说明

本项目代码使用腾讯邮箱

自己给自己发邮件，所以邮件发送的帐号、授权码请填你自己的

腾讯邮箱密码非邮箱登录密码而是授权码，具体获取方式见下图

![](11111.png)